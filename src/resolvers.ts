import { pubsub } from './server';
import {getGamesData} from "./services/connection";

export const GAME_UPDATED = 'GAME_UPDATED';

export const resolvers = {
    Subscription: {
        gameUpdate: {
            subscribe: () => {
                getGamesData();
                return pubsub.asyncIterator(['GAME_UPDATED'])
            },
        },
    }
};
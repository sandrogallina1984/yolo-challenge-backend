import WebSocket, {RawData} from 'ws';
import {Game} from "../generated/graphql";
import { pubsub } from '../server';
import {GAME_UPDATED} from "../resolvers";

export type SanitizedMessage = {
    type: string,
    game: Game
}

export const getGamesData = async () => {
    const ws = new WebSocket('ws://sb-ws-mock-api.herokuapp.com/?username=testuser11&password=secret11');
    await ws.on('open', function open() {
            ws.send('{"type": "recovery"}');
        })
        .on('message', async (data) => {
        if ((data + '') != 'Connection susccess. Listening to incoming messages...') {
            const sanitizedMessage = sanitizeMessage(data);
            if (sanitizedMessage) {
                return await pubsub.publish(GAME_UPDATED, {gameUpdate: sanitizedMessage.game});
            }
        }

        return [''];
    });
}

const sanitizeMessage = (message: RawData): SanitizedMessage | null => {
    let messageJson = null;
    try {
        messageJson = JSON.parse(message + '');
    } catch (e) {
        return null;
    }

    const {payload} = messageJson;
    return {
        type: messageJson.type,
        game: {
            id: payload.id,
            date: payload.startTime,
            category: {
                id: payload.category.id,
                name: payload.category.slug
            },
            home: {
                id: payload.competitors[0].id,
                name: payload.competitors[0].name,
                score: payload.competitors[0].score,
            },
            away: {
                id: payload.competitors[1].id,
                name: payload.competitors[1].name,
                score: payload.competitors[1].score,
            },
            odds: {
                home: payload?.markets[0]?.selections[0].odds || '',
                draw: payload?.markets[0]?.selections[1].odds || '',
                away: payload?.markets[0]?.selections[2].odds || ''
            }
        }
    };
}
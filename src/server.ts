import fs from "fs"
import path from 'path'
import express from "express"
import http from 'http';
import { ApolloServer, PubSub } from 'apollo-server-express'
import { resolvers } from './resolvers'

export const pubsub = new PubSub();

const typeDefs = fs.readFileSync(path.join(__dirname, "../schema.graphql"), "utf8").toString()
const server = new ApolloServer({ typeDefs, resolvers })

const app = express()
server.applyMiddleware({ app })

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
)